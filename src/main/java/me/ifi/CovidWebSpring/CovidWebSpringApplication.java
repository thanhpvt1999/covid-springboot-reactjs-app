package me.ifi.CovidWebSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidWebSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidWebSpringApplication.class, args);
	}

}
